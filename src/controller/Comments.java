package controller;

import com.google.gson.Gson;
import database.DBConnector;
import model.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by kcp on 5/22/17.
 */
public class Comments extends HttpServlet {
    //doPost is called when user comments in a post through AJAX
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //database connection
        Connection con = DBConnector.getConnection(request, response);
        //getting user object from session
        Users user = (Users) request.getSession().getAttribute("loggedInUser");
        PrintWriter out = response.getWriter();

        //if user is not logged in redirected to login page
        if (user == null) {
            response.sendRedirect("/index.jsp");
            return;
        }

        String user_id = user.getUserid();//getting userid form session
        String post_id = request.getParameter("post_id");//getting post id from AJAX call
        String comment = request.getParameter("comment");//getting comment form AJAX call


        HashMap<String, String> jsonData = new HashMap<>();//hash map to store status
        try {
            String sql = "insert into comments (userid, postid, comment) values (?,?,?)";//database query to add comment
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user_id);
            ps.setString(2, post_id);
            ps.setString(3, comment);
            int res = ps.executeUpdate();

            if (res > 0) {
                //if added in database status of success is set
                jsonData.put("status", "success");
            } else
                //if was not added in database status of failed is set
                jsonData.put("status", "failed");

            Gson gson = new Gson();//new Gson object created
            response.setContentType("application/json");
            out.print(gson.toJson(jsonData));//hashmap is converted into json object using gson
            out.flush();//buffer memory cleared
            request.setAttribute("post_id", post_id);//attribute set in a request
            request.setAttribute("comment-value", comment);//attribute set in a request
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
